# PicSel

PicSel is a photo library application written in Rust. The crate is separated into a backend library that manages photo libraries using SQL databases via [`rusqlite`](https://github.com/rusqlite/rusqlite) and a frontend that displays the photos using [`egui`](https://github.com/emilk/egui) for the UI.

## Licensing

Project available under GPLv3. See `LICENSE` for full license text. The `egui` crate is available under Apache 2.0 or MIT at your option. The `rusqlite` crate is available under MIT.
