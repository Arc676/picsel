// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3)

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// Based on code in https://github.com/emilk/eframe_template

use eframe::egui::{Context, Ui};
use eframe::{egui, Frame};

use picseldb::library::PhotoLibrary;

#[forbid(unsafe_code)]

macro_rules! labeled_field {
    ($ui:ident, $lbl:tt, $target:expr) => {
        $ui.horizontal(|$ui| {
            $ui.label($lbl);
            $ui.text_edit_singleline($target);
        });
    };
}

#[derive(Default, serde::Serialize, serde::Deserialize)]
struct PicSel {
    #[serde(skip)]
    library: Option<PhotoLibrary>,
    #[serde(skip)]
    library_error: Option<String>,

    library_path: String,
}

#[derive(PartialEq)]
enum LibraryOperation {
    NoOp,
    Open,
    Create,
}

impl PicSel {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }
        Default::default()
    }

    fn file_menu(&mut self, ui: &mut Ui) -> bool {
        let mut op = LibraryOperation::NoOp;
        let mut exit = false;
        ui.menu_button("File", |ui| {
            if ui.button("Create").clicked() {
                op = LibraryOperation::Create;
            }
            if ui.button("Open").clicked() {
                op = LibraryOperation::Open;
            }

            ui.separator();
            exit = ui.button("Quit").clicked();
        });
        if exit {
            return true;
        }
        if op != LibraryOperation::NoOp {
            let dialog = rfd::FileDialog::new()
                .add_filter("Database", &["db"]);
            let picked = match op {
                LibraryOperation::Create => dialog.save_file(),
                LibraryOperation::Open => dialog.pick_file(),
                _ => panic!("Impossible state"),
            };
            if let Some(path) = picked {
                self.library_path = path.display().to_string();
            } else {
                return false;
            }
            let res = match op {
                LibraryOperation::Create => PhotoLibrary::new(&self.library_path),
                LibraryOperation::Open => PhotoLibrary::open(&self.library_path),
                _ => panic!("Impossible state"),
            };
            match res {
                Ok(library) => {
                    self.library = Some(library);
                    self.library_error = None;
                }
                Err(e) => self.library_error = Some(e.to_string()),
            }
        }
        // TODO: show error dialog
        false
    }

    fn library_menu(&mut self, ui: &mut Ui) {
        if self.library.is_none() {
            return;
        }
        let library = self.library.as_mut().unwrap();
        ui.menu_button("Library", |ui| {
            if ui.button("Import files...").clicked() {
                if let Some(files) = rfd::FileDialog::new().pick_files() {
                    library.import_entries(&files);
                }
            }
            if ui.button("Import directories...").clicked() {
            }
        });
    }


    fn menu(&mut self, ui: &mut Ui) -> bool {
        let mut exit = false;
        egui::menu::bar(ui, |ui| {
            exit = self.file_menu(ui);
            self.library_menu(ui);
        });
        exit
    }

    fn control_panel(&mut self, ui: &mut Ui) {
        ui.heading("PicSel");
    }

    fn gallery(&mut self, ui: &mut Ui) {
        if self.library.is_none() {
            return;
        }
        let library = self.library.as_ref().unwrap();
        if let Ok(entries) = library.all_entries() {
            for entry in entries {
                ui.label(format!(
                    "{}: {}",
                    entry.id(),
                    entry.filename().to_str().unwrap()
                ));
                ui.end_row();
            }
        }
    }
}

impl eframe::App for PicSel {
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        egui::TopBottomPanel::top("menu_bar").show(ctx, |ui| {
            if self.menu(ui) {
                frame.close();
            }
        });
        egui::SidePanel::left("control_panel").show(ctx, |ui| {
            self.control_panel(ui);
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::Grid::new("gallery").show(ui, |ui| self.gallery(ui))
        });
    }

    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }
}

fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "PicSel",
        native_options,
        Box::new(|cc| Box::new(PicSel::new(cc))),
    );
}
