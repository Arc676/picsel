// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3)

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use std::path::{Path, PathBuf};
use rusqlite::{Connection, Row};

pub struct PhotoLibrary {
    db: Connection,
}

pub struct Entry {
    id: u32,
    filename: PathBuf,
}

impl PhotoLibrary {
    pub fn open<P: AsRef<Path>>(path: P) -> rusqlite::Result<Self> {
        Ok(PhotoLibrary {
            db: Connection::open(path)?,
        })
    }

    pub fn new<P: AsRef<Path>>(path: P) -> rusqlite::Result<Self> {
        let library = PhotoLibrary::open(path)?;
        library.db.execute("CREATE TABLE Library(ID integer primary key autoincrement, Filename varchar(255))", [])?;
        Ok(library)
    }

    pub fn import_entries(&mut self, paths: &[PathBuf]) -> rusqlite::Result<()> {
        for p in paths {
            let mut stmt = self.db.prepare_cached("INSERT INTO Library VALUES (null, ?)")?;
            stmt.execute([p.display().to_string()])?;
        }
        Ok(())
    }

    pub fn all_entries(&self) -> rusqlite::Result<Vec<Entry>> {
        let mut stmt = self.db.prepare_cached("SELECT * FROM Library")?;
        let entries = stmt.query_map([], Entry::from_row)?;
        entries.collect()
    }
}

impl Entry {
    fn from_row(row: &Row) -> rusqlite::Result<Self> {
        Ok(Entry {
            id: row.get(0)?,
            filename: PathBuf::from(row.get::<usize, String>(1)?),
        })
    }

    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn filename(&self) -> &PathBuf {
        &self.filename
    }
}
